# Chain of Responsibility Pattern

The repository contains a Java application for Chain of Responsibility Pattern example.

The chain of responsibility pattern creates a chain of receiver objects for a request. This pattern decouples sender and receiver of a request based on type of request. This pattern comes under behavioral patterns..

## Built With

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java SE Development Kit 
* [IntelliJIDEA](https://www.jetbrains.com/idea/) - IDE for Java
* [Git](https://git-scm.com/) - The distributed version control system

## Author

* **María Ruth Vargas**